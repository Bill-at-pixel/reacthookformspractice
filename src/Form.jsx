import { useState } from "react"
import { useForm } from "react-hook-form"
import UserDetails from "./UserDetails"
import Card from "./Styles/Card"

const Form = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();

    // These are the arguments you can use for the register function
    const registerArgs = {
        required: true,
        maxLength: 8, 
        message: "optional error message",
        valueAsNumber: true,
        valueAsDate: true
    }
    console.dir(errors)
    console.log("^ errors")

    const doSubmit = (data) => {
        console.log(data)
        console.log("^ submitted data")
        setFirstName(data.userFirstName)
        setLastName(data.userLastName)
        setGender(data.userGender)
        setAge(data.userAge)
    }
    // handleSumbit takes a function for an argument, uninvoked, and without any parameters
    // The function inside handleSubmit (in this case doSubmit) will then have access to the form info
    // from the 'data' parameter
    
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [age, setAge] = useState()
    const [gender, setGender] = useState()

    return(
        <>
        <Card gender={gender}>
            <form onSubmit={handleSubmit(doSubmit)}>
                        <label htmlFor="first name">First name: </label><br/>
                        <input {...register("userFirstName", {required: false})} id="firstname"/><br/>
                        {errors.firstName && <span>This field is required</span>}
                        <label htmlFor="last name">Last name: </label><br/>
                        <input {...register("userLastName", {required: false})} id="lastname"/><br/>
                        <label htmlFor="age">Age: </label><br/>
                        <input {...register("userAge")} id="age"/><br/>
                        <label htmlFor="gender"></label><br/>
                    <select {...register("userGender")} id="gender">
                        <option value="">Select:</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select><br/>
                    <p>Do you work from home?</p>
                    <div>
                        <label>
                            <input type="radio" value="yes" name="radio"/>
                            Yes
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio" value="no" name="radio"/>
                            No
                        </label>
                    </div>
                    <br/>
                    <br/>
                <input type="submit"/>
            </form>
        </Card>
            <br/>
            <UserDetails firstName={firstName} lastName={lastName} age={age} gender={gender}/>
            </>
    )
}

export default Form;