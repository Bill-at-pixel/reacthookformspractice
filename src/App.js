import './App.css';
import Form from "../src/Form"
import styled from "styled-components"

function App() {
  return (
    <StyledApp>
      <h1>Please enter your details below</h1>
      <Form/>
    </StyledApp>
  );
}

const StyledApp = styled.div`
  text-align: center;
`

export default App;
