import styled from "styled-components"

const Card = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${props => {
        if (props.gender === "male") return "#ff8484"
        else if (props.gender === "female") return "#ffec58;"
        else return "#a8ffa8";
    }};
    border: 2px;
    box-shadow: grey 9px 9px 7px;
    border-radius: 2px;
    width: 30%;
    margin: auto;
    padding: 1rem;
`

export default Card