import Card from "./Styles/Card"

const UserDetails = (props) => {

    console.dir(props)
    return(
        <Card gender={props.gender}>
            <h2>Here are your details</h2>
            <p>Your first name: {props.firstName}</p>
            <p>Your last name: {props.lastName}</p>
            <p>Your gender: {props.gender}</p>
            <p>Your age: {props.age}</p>
        </Card>
    )
}

export default UserDetails